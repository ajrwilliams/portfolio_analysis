"""
portfolio:
A module to perform simple analysis of efficient portfolios and the associated uncertainties
"""

import matplotlib
matplotlib.use('TkAgg') 
import numpy as np
import pandas as pd
import scipy as sc
from scipy import stats 
import pandas.io.data as web
import datetime
import time
import matplotlib.pyplot as plt

__author__ = "Andrew Williams"
__copyright__ = "Copyright 2016"
__email__ = "ajr.williams@hotmail.co.uk"

def get_monthly_data(slist, start=datetime.datetime(2010, 1, 1), end=datetime.datetime.today()):
    """
    Gets the monthy return data from Yahoo finance for list of securities and returns them as a dataframe

    slist - list of names of securities to get data for
    start - start time for data set
    end   - end time for data set
    """
    r = pd.DataFrame()
    for i in slist:
        f = web.get_data_yahoo(i, start=start, end=end,interval='m')
        r[i] = f["Adj Close"]
    return r

def min_variance_portfolio(means, covar, shortsales=False):
    """
    Computes the minimum variance portfolio in the CER model
    
    means      - mean returns of the securities as a series
    covar      - Covariance matrix as a series
    shortsales - Are short sales allowed as boolean

    Returns a tuple (dataframe with portfolio shares for each secruity, portfolio mean return, portfolio sd)
    """
    n = len(means)
    if shortsales:
        #use Lagrange method setting up system of linear equations
        A = np.concatenate((2*covar.as_matrix(), np.ones(n).reshape(n, 1)), 1)
        A = np.concatenate((A , np.append(np.ones(n),0).reshape(1, n+1)) , 0)
        b = np.append(np.zeros(n), 1.)
        x =np.linalg.solve(A, b)
        r = np.reshape(x[:n], (1,n))
        return pd.DataFrame(r, columns=means.index), np.dot(means, r.T)[0], np.sqrt(np.dot(r, np.dot(covar, r.T)))[0][0]
    else:
        #use scipy to minimize portfolio variance with constraints
        from scipy.optimize import minimize as minimize
        cons = ({'type': 'eq', 'fun': lambda x:  np.sum(x) - 1.0})
        x0 = np.ones(n)/n
        s = lambda x : np.dot(x, np.dot(covar, x))
        bnds = ()
        for i in range(n):
            bnds = ((0,1),) + bnds 
        res = minimize(s, x0, method='SLSQP', bounds=bnds, jac=False,  constraints=cons, tol=1e-8)
        r = np.reshape(res.x, (1,n))
        return pd.DataFrame(r, columns=means.index), np.dot(means, r.T)[0], np.sqrt(np.dot(r, np.dot(covar, r.T)))[0][0]
        
def portfolio_by_return(means, covar, target=-1, shortsales=False):
    """
    Computes an efficient portfolio in the CER model with expected return equal to mean

    means      - mean returns of the securities as a series
    covar      - Covariance matrix as a series
    target     - Target return of portfolio, if unspecified or -1 target return will be largest return of single security.
    shortsales - Are short sales allowed as boolean

    Returns a tuple (dataframe with portfolio shares for each secruity, portfolio mean return, portfolio sd)
    """
    if (target == -1): target = np.max(means)
    n = len(means)
    if shortsales:
       #use Lagrange method setting up system of linear equations
        tmp =  np.concatenate((means.as_matrix().reshape(1, n), np.zeros((n-1, n))))
        A = np.concatenate((2*covar.as_matrix(), means.as_matrix().reshape(n,1), np.ones(n).reshape(n, 1)), 1)
        tmp = np.concatenate((means.as_matrix().reshape(1, n), np.zeros((1, 2))), 1)
        A = np.concatenate((A , tmp,np. append(np.ones(n), np.zeros(2)).reshape(1, n+2)))
        b = np.append(np.append(np.zeros(n), np.ones(1)*target), 1.)
        x =np.dot(np.linalg.pinv(A), b)
      
        r = np.reshape(x[:n], (1,n))
        return pd.DataFrame(r, columns=means.index), np.dot(means, r.T)[0], np.sqrt(np.dot(r, np.dot(covar, r.T)))[0][0]
       
    else:
        #use scipy to minimize portfolio variance with constraints
        from scipy.optimize import minimize as minimize
        cons = ({'type': 'eq', 'fun': lambda x:  np.sum(x) - 1.0}, {'type': 'eq', 'fun': lambda x:  np.dot(x, means.as_matrix()) - target})
        x0 = np.random.dirichlet(np.ones(n), 1)
        s = lambda x : np.dot(x, np.dot(covar, x))
        bnds = ()
        for i in range(n):
            bnds = ((0.0,1.0),) + bnds 
        res = minimize(s, x0, method='SLSQP', bounds=bnds, jac=False,  constraints=cons, tol=1e-10)
        
        r = np.reshape(res.x, (1,n))
        return pd.DataFrame(r, columns=means.index), np.dot(means, r.T)[0], np.sqrt(np.dot(r, np.dot(covar, r.T)))[0][0]


def generate_boot_sample(f, data, n_boot=1000, f_scalar=[], scalar_names=[]):
    """
    Generates a bootstrap sample of a list of observables
    generation is split into sub blocks of size ~1000 
    Note different observables are calculated from the same samples so different observables retain correlation.

    f            - list of functions that calculate the observable given a data sample, should take and return a dataframe
    data         - the original sample data as a dataframe
    n_boot       - the number of bootstrap samples to generate
    f_scalar     - List of scalar functions that return a single value per bootstrap sample (optional)
    scalar_names - List of names of the scalar function (optional)
    Returns a list of dataframes, one for each observable with a bootstrap sample for each security and optionally a dataframe containing the results of the scalar functions.
    """
    #split total number of samples into blocks of ~1000
    s = n_boot / 1000
    samples = [1000]*np.floor(s) + [n_boot % 1000]
    if 0 in samples :
        samples.remove(0)
    sample = [1]*(len(f))
    scalars=None
    #set up dataframes to take bootstrap samples
    for i in range(len(f)):
        sample[i] = pd.DataFrame(None, columns=data.columns)   
    if (len(f_scalar) > 0):
        scalars = pd.DataFrame(None, columns=scalar_names)
    #generate sub samples and append to dataframes
    for i in samples:
        if (len(f_scalar) > 0):
            tmp, scalar = generate_boot_sample_block(f, data, n_boot=i, f_scalars=f_scalar, scalar_names=scalar_names)
            scalars = scalars.append(scalar, ignore_index=True)
        else:
            tmp, _ = generate_boot_sample_block(f, data, n_boot=i)
            
        
        for j in range(len(f)):
            try:
                sample[j] = sample[j].append(tmp[j], ignore_index=True)
            except:
                print "j = ", j
                print "tmp = ", np.shape(tmp)
                print 'sample = ', len(sample)
                exit()

    return sample, scalars

def generate_boot_sample_block(f_tmp, data, n_boot=1000, f_scalars=[], scalar_names=[]):
    """
    Generate a subblock of the bootstrap sample, used internally user should call generate_boot_sample instead.
    """
    z = lambda x: x(sample)
    n = len(data)
    #Create one large bootstrap sample in one shot as this is faster
    big_sample = data.sample(n*n_boot, replace=True)
    #take first sample as special case to start populating array
    sample = big_sample[:n]       
    sample.index = data.index
    boot_samples = [1]*len(f_tmp)
    scalars = None
    for i in range(len(f_tmp)):
        boot_samples[i] = f_tmp[i](sample).as_matrix()
    if (len(f_scalars) > 0):
        scalars = np.array(map(z, f_scalars)).reshape(1, len(f_scalars))
    #loop over slices of large sample
    for i in range(1, n_boot):
        sample = big_sample[i*n:i*n+n]
        sample.index = data.index
        #loop over observable functions and append to the bootstrap sample
        for i in range(len(f_tmp)):
            boot_samples[i] = np.vstack((boot_samples[i], f_tmp[i](sample).as_matrix()))   
        if (len(f_scalars) > 0 ) :
            scalars = np.vstack((scalars,np.array(map(z, f_scalars)).reshape(1, len(f_scalars))))
    #set names and return as a dataframe
    for i in range(len(f_tmp)):
        boot_samples[i] = pd.DataFrame(boot_samples[i], columns=data.columns)   
        if (i == 2):
            print boot_samples[i]
    print "returning boot_samples ", np.shape(boot_samples)
    print "last one is ", boot_samples[2]
    print "type is ", type(boot_samples)
    return boot_samples, pd.DataFrame(scalars, columns=scalar_names)

def risk_return_asset_plot(means, sds, savefig=None, showfig=True, mean_errors=None, sd_errors=None, errors=False):
    """
    Generates a risk-return plot of the listed securities. 
    
    means       - Mean returns of securities as a series
    sd          - standard deviations of the returns as a series
    savefig     - (default:None) file path of save figure
    showfig     - boolean display generated figure or not
    mean_errors - errors on mean returns as a series
    sd_errors   - errors on standard deviations as a series
    errors      - boolean: plot error bars or not
    """
    
    if (errors):
        for i, v in means.iteritems():
            xerrs=sd_errors[i]
            yerrs=mean_errors[i]
            plt.errorbar(sds[i], means[i],  xerr=xerrs, yerr=yerrs)        
    else:
        plt.plot(sds, means, "8")
    for i, v in means.iteritems():

        plt.annotate(i, xy=(sds[i], means[i]), xycoords='data', xytext=(5, 5), textcoords='offset points')
    plt.xlabel('Standard deviation')
    plt.ylabel('Return')
    if (savefig != None):
        plt.savefig(savefig)
    if (showfig == True): plt.show()
    
def risk_return_contour_plot(means, sds, savefig=None, showfig=True):
    """
    Plot 68% and 95% contours of securities in standard deviation-mean plane.
    Using a bootstrap sample contours a plotted emperically without assuming a distribution.
    
    means    - a bootstrap sample of mean returns for each security as a dataframe
    sds      -  a bootstrap sample of standard deviations for each security as a dataframe
    savefig  - (default:None) file path of save figure
    showfig  - boolean display generated figure or not
    """
    for i, v in means.iteritems():
        #bin the bootstrap samples in 2 dimensions
        h = np.histogram2d(sds[i], means[i],bins=[30,30], \
                   range=[[sds[i].min(), sds[i].max()],\
        [means[i].min(), means[i].max()]])
        hist, bins_x, bins_y = h
        x = np.array([])
        y = np.array([])
        z = np.array([])
        #arrange data into 3 arrays
        for i in range(len(hist)):
            for j in range(len(hist[i])):
                x=np.append(x, (bins_x[i+1]+bins_x[i])/2)
                y=np.append(y, (bins_y[j+1]+bins_y[j])/2)
                z=np.append(z, hist[i,j])
        # Interpolate using scipy library
        from scipy import interpolate
        from scipy import stats
        rbf = interpolate.Rbf(x, y, z, function='linear')
        
        #create a coarse grid and interpolate for contour plot
        xi, yi = np.linspace(bins_x.min(), bins_x.max(), 30), np.linspace(bins_y.min(), bins_y.max(), 30)
        xi, yi = np.meshgrid(xi, yi)
        zi = rbf(xi, yi)

        #create a fine grid for plotting
        xi, yi = np.linspace(bins_x.min(), bins_x.max(), 120), np.linspace(bins_y.min(), bins_y.max(), 120)
        xi, yi = np.meshgrid(xi, yi)

        #find the contour levels for 68% and 95%
        hist = hist.flatten()
        hist.sort()
        sumed = hist.sum()
        tmp = 0
        for i in range(len(hist)):
            tmp = tmp + hist[i]
            if (tmp > 0.32*sumed):
                c1 = (hist[i] + hist[i-1])/2.
                break
        tmp = 0
        for i in range(len(hist)):
            tmp = tmp + hist[i]
            if (tmp > 0.05*sumed):
                c2 = (hist[i] + hist[i-1])/2.
                break
        lims = [c1,c2]

        #use smooth zooming from coarse to fine grid to create final plot
        import matplotlib.pyplot as plt       
        import scipy.ndimage
        data = scipy.ndimage.zoom(zi, 4)
        plt.contour(xi, yi, data, lims)
    if (showfig): plt.show()  
    
def get_portfolio_properties(means, covar, portfolio):
    """
    Return the mean and standard deviation of the portfolio return 

    means     - mean returns of securities
    covar     - covariance matrix of security returns
    portfolio - specification of portfolio weights

    Returns a tuple (mean, stdev)
    """
    mean = np.dot(means, portfolio.T)[0]
    sd = np.sqrt(np.dot(portfolio, np.dot(covar, portfolio.T)))[0][0]
    return mean, sd

def plot_portfolios(means_data, covar_data, shortsales=False, showfig=True, savefig=None, plotcommands="-r"):
    """
    Plots the efficient portfolio frontier given the data with or without shortsales. If shortsales are allowed a convex combination
    of two efficient portfolios is plotted. Without shortsales the efficient frontier is found for a sample of fixed returns.
    
    means_data   - mean returns of securities
    covar_data   - covariance matrix of returns
    shortsales   - Allow short sales
    showfig      - Display result to screen
    savefig      - filename to save figure if required
    plotcommands - plot style to pass to matplotlib (default "-r" is a red line)
    """
    #get min variance portfolio
    min_var = min_variance_portfolio(means_data, covar_data, shortsales=shortsales)[0]
    mean_, sd_ = get_portfolio_properties(means_data, covar_data, min_var)

    if (shortsales == False):
        means = np.array([])
        sd = np.array([])
        #if no short sales first only plot between the two portfolios
        for target in np.linspace(mean_, means_data.max(), 100):
            port, mean_, sd_ = portfolio_by_return(means_data, covar_data, target=target, shortsales=shortsales)
            means = np.append(means, mean_)
            sd = np.append(sd, sd_)
        #try extending the plot
        for target in np.linspace(means_data.max(), means_data.max()*2, 100):
            port, mean_, sd_ = portfolio_by_return(means_data, covar_data, target=target, shortsales=shortsales)
            if ((port.as_matrix() > 0).all()):
                 means = np.append(means, mean_)
                 sd = np.append(sd, sd_)                
    else:
        max = 2
        max_port = portfolio_by_return(means_data, covar_data, target=-1, shortsales=shortsales)[0]
        #loop over convex combinations of portfolios
        means = np.array([])
        sd = np.array([])
        for alpha in np.linspace(0, max, 100):
            port = (1-alpha)*min_var + alpha*max_port
            mean_, sd_ = get_portfolio_properties(means_data, covar_data, port)
            means = np.append(means, mean_)
            sd = np.append(sd, sd_)

    plt.plot(sd,means, plotcommands)
    if (savefig != None):
        plt.savefig(savefig)
    if (showfig):
        plt.show()

def plot_portfolio_weights(port, errorbars=False,uncertainties=None, showfig=True, savefig=None, title="Portfolio weightings"):
    """
    Plot the portfolio weights in a bar chart
    
    port          - The portfolio weights in a dataframe
    errorbars     - Plot errorbars, if true error bars should be given in argument uncertainties
    uncertainties - The uncertainties on the portfolio weights 
    showfig       - Display result to screen
    savefig       - filename to save figure if required
    title         - specify a title for the plot
    """
    if (errorbars==False):
        plt.bar(np.arange(len(port.columns)), port.as_matrix()[0],align='center')
    else :
        plt.bar(np.arange(len(port.columns)), port.as_matrix()[0],align='center', yerr=uncertainties.as_matrix(), ecolor='k')
    plt.xticks(np.arange(len(port.columns)), port.columns)
    plt.xlabel('Asset')
    plt.ylabel('Portfolio Weight')
    plt.title(title)
    if (savefig != None) : plt.savefig(savefig)
    if (showfig) : plt.show()
    
def portfolio_VaR(returns, port, alpha=0.05):
    """
    Returns the portfolio Value at Risk (VaR) at the alpha% level. Based on the empricial distribution of returns
    
    returns - Returns data for securities
    port    - The portfolio weights
    alpha   - The % level to calculate VaR
    """

    portfolio_returns =np.dot(returns, port.T)
    return np.percentile(portfolio_returns,alpha)

def portfolio_model_VaR(means, covar, portfolio, alpha=0.05):
    """
    Return the portfolio Value at Risk (VaR) at the alpha% level. Based on the CER model parameters assuming a normal distribution of returns.
    
    means      - Mean returns of the assets
    covar      - Covariance matrix of assets
    portforlio - Portfolio weights
    alpha      - The % level to calculate VaR
    """
    mean, sd = get_portfolio_properties(means, covar, portfolio)
    return sc.stats.norm.ppf(alpha, mean, sd)


def get_portfolio_frontier(means_data, covar_data, shortsales=False, grid=100):
    """ 
    Returns list of portfolios along the portfolio frontier starting from the minimum variance portfolio and increasing the mean return.

    means_data  - mean returns of assets
    covar_data  - covariance matrix of asset returns
    shortsales  - allow short sales
    grid        - number of portfolios to return
    """
    min_var, mean_, std_ = min_variance_portfolio(means_data, covar_data, shortsales=shortsales)
    if (shortsales == False):
        portfolios = min_var
        #if no short sales first only plot between the two portfolios
        for target in np.linspace(mean_, means_data.max(), grid-1):
            portfolios = portfolios.append(portfolio_by_return(means_data, covar_data, target=target, shortsales=shortsales)[0])
               
    else:
        portfolios = pd.DataFrame(None)
        max = 2
        max_port = portfolio_by_return(means_data, covar_data, target=-1, shortsales=shortsales)[0]
        #loop over convex combinations of portfolios
        means = np.array([])
        sd = np.array([])
        for alpha in np.linspace(0, max, grid):
            portfolios = portfolios.append((1-alpha)*min_var + alpha*max_port)
    return portfolios

def plot_portfolio_bands(returns, shortsales=True, grid=100, n_boot=1000, portfolio_from_sample=False):
    """
    Plots countours of 68% and 95% CL in the (std deviation, mean return) plane
    Portfolio frontier is created as normal, a bootstrap sample for each portfolio on the frontier is generated and used to estimate the 68% and 95% limits on the mean return.
    If portfolio from sample is enabled portfolio frontier is recalculated for each bootstrap sample. This can be slow especially if no short sales allowed.
    
    returns               - Return data
    shortsales            - Allow short sales
    grid                  - Number of points along frontier to evaluate
    n_boot                - Number of bootstrap samples to calculate
    portfolio_from_sample - Recalculate portfolio weights for each bootstrap sample (uncertainty then calculated on std deviation.    
    """
    means_ = returns.mean()
    covar_ = returns.cov()
    n = len(returns)
    tmp_means_ = np.reshape(means_.T, (1, len(means_)))
  
  
    if (portfolio_from_sample == False):
        frontier = get_portfolio_frontier(means_, covar_, shortsales, grid)
        print frontier.T
        vars = np.sqrt(np.diag(np.dot(frontier, np.dot(covar_, frontier.T))))
        f_means = np.dot(tmp_means_, frontier.T)[0]
    
        #make a custom bootstrap sample
        s = n_boot / 1000
        samples = [1000]*np.floor(s) + [n_boot % 1000]
        if 0 in samples:
            samples.remove(0)
        print samples
        columns = ["p_" + str(x) for x in range(0, grid)]
        total_means = pd.DataFrame(None, columns=columns)
        
        for s in samples:
            big_sample = returns.sample(n*s, replace=True)
            sample = big_sample[:n]   
            means_ = sample.mean()
            means_ = np.reshape(means_.T, (1, len(means_)))
            tmp_means = np.dot(means_, frontier.T)
            boot_means = tmp_means
            for i in range(1, s):
                sample = big_sample[i*n:i*n+n]
                means_ = sample.mean()
                means_ = np.reshape(means_.T, (1, len(means_)))
                tmp_means = np.dot(means_, frontier.T)
                boot_means = np.vstack((boot_means, tmp_means))
            total_means = total_means.append(pd.DataFrame(boot_means, columns=columns))
        
        #calculate 68% and 95% bands for each portfolio
        low_95 = total_means.quantile(0.025)
        low_68 = total_means.quantile(0.32/2.)
        high_68 = total_means.quantile(1 - 0.32/2.)
        high_95 = total_means.quantile(1- 0.025)
        plt.plot(vars, low_68, '--k')
        plt.plot(vars, high_68, '--k')
        plt.plot(vars, low_95, '-k')
        plt.plot(vars, high_95, '-k')
        plt.plot(vars, f_means, '-g')
    if (portfolio_from_sample):
        #generate list of target returns
        frontier = get_portfolio_frontier(means_, covar_, shortsales, grid)
        #drop first term as it is minimum variance portfolio
        f_means = np.dot(tmp_means_, frontier.T)[0]
        f_vars = np.sqrt(np.diag(np.dot(frontier, np.dot(covar_, frontier.T))))
        #for each bootstrap sample generate target portfolios and retain the variances
        #make a custom bootstrap sample
        s = n_boot / 1000
        samples = [1000]*np.floor(s) + [n_boot % 1000]
        if 0 in samples:
            samples.remove(0)
        print samples
        columns = ["p_" + str(x) for x in range(0, grid)]
        total_stds = pd.DataFrame(None, columns=columns)
        for s in samples:
            big_sample = returns.sample(n*s, replace=True)
            sample = big_sample[:n]   
            means_ = sample.mean()
            covar_ = sample.cov()
            frontier_ = get_portfolio_frontier(means_, covar_, shortsales, grid)
            vars = np.sqrt(np.diag(np.dot(frontier, np.dot(covar_, frontier.T))))
            for i in range(1, s):
                sample = big_sample[i*n:i*n+n]
                means_ = sample.mean()
                covar_ = sample.cov()
                frontier_ = get_portfolio_frontier(means_, covar_, shortsales, grid)
                vars = np.vstack((vars, np.sqrt(np.diag(np.dot(frontier, np.dot(covar_, frontier.T))))))
                print "sub sample %d done" % i
            total_stds = total_stds.append(pd.DataFrame(vars, columns=columns))              
        #calculate 68% and 95% bands for each portfolio
        low_95 = total_stds.quantile(0.025)
        low_68 = total_stds.quantile(0.32/2.)
        high_68 = total_stds.quantile(1 - 0.32/2.)
        high_95 = total_stds.quantile(1- 0.025)
        plt.plot(low_68, f_means, '--k')
        plt.plot(high_68,  f_means, '--k')
        plt.plot(low_95,  f_means, '-k')
        plt.plot(high_95,  f_means, '-k')
        plt.plot(f_vars,  f_means, '-g')
     
def invest_value(returns_set):
    """
    Calculates value of a $1 investment, given the set of returns data.

    returns_set - Dataframe containing the returns data.

    returns a dataframe containing the investment values.
    """
    #populate value of $1 investment over lifetime
    #fill missing data
    returns_set = returns_set.fillna(returns_set.mean())
    tmp = [([1] * len(returns_set.columns))] * len(returns_set)
    prices = pd.DataFrame(tmp, columns=returns_set.columns, index=returns_set.index)
    old_index = returns_set.index[0]
    for index, row in returns_set.iterrows():
        if (index != old_index):
            prices.loc[index] = prices.loc[old_index] * (np.exp(returns_set.loc[index]))
        old_index = index
    return prices   

def drawdown(returns):
    """
    Calculates the largest drawdowns and largets updraws (trough to peak) in the sample.
    Also returns the length of the drawdowns and longest recovery periods.
    
    returns - Dataframe containing returns data. value of investment calculated by invest_value function.

    returns DataFrame containing largest drawdown, length of drawdown and longest recovery period
    """
    prices = invest_value(returns)
    dds = pd.DataFrame(None , index=prices.columns)
    peak = dict()
    trough = dict()
    peakdate = dict()
    troughdate = dict()
    dd = dict()
    ud = dict()
    dd_date = dict()
    ud_date = dict()
    rec_len = dict()
    for  index, row in prices.iterrows():
        for asset, val in row.iteritems():
            #check if new peak is reached
            if (row[asset] > peak.get(asset, 0)):
                #asset has hit a new peak
                if (index - peakdate.get(asset, index) >= rec_len.get(asset,index - peakdate.get(asset, index) )):
                    #update max recovery time based on new peak
                    rec_len[asset] = index - peakdate.get(asset, index)
                peak[asset] = row[asset]
                peakdate[asset] = index
                #reset trough to only count forward from peak
                trough[asset] = row[asset]
                troughdate[asset] = index
            #check if new trough is reached
            if (row[asset] < trough.get(asset, 1e10)):
                trough[asset] = row[asset]
                troughdate[asset] = index
            #update max drawdown based on current peak and trough
            if (peak[asset] - trough[asset] > dd.get(asset, 0)):
                dd[asset] = peak[asset] - trough[asset]
                dd_date[asset] = troughdate[asset] - peakdate[asset]
    dds['DrawDown'] = pd.Series(dd)
    dds['DrawDown_length'] = pd.Series(dd_date)
    dds['Longest_recovery'] = pd.Series(rec_len)
    return dds

    
def asset_properties(returns, uncertainties=True, risk_free=0.002):
    """
    Prints a table summarising the properties of the assets or portfolios

    returns       - set of returns data as a dataframe
    uncertainties - Calculate uncertainies using bootstrap method
    risk_free     - Specify a riskfree rate to calculate Sharpe's ratio
    """

    means = returns.mean()
    var = returns.std()
    skew = returns.skew()
    kurt = returns.kurtosis()
    JB = len(returns-1)/6. * (skew*skew + 0.25*(kurt-3)**2)
    chi2 = sc.stats.chi2(2)
    is_norm = JB < chi2.ppf(0.95)
    VaR = returns.quantile(0.05)
    dds = drawdown(returns)
    l_ret = returns.max()
    l_DD = dds['DrawDown']
    l_rec = dds['Longest_recovery']

    if (uncertainties):
    #get uncertainties
        f_mean = lambda x: x.mean()
        f_var =lambda x: x.var()
        f_skew =lambda x: x.skew()
        f_kurt = lambda x: x.kurt()
        f_VaR = lambda x: x.quantile(0.05)
        f_dd = lambda x: pd.DataFrame(np.reshape(drawdown(x)['DrawDown'].as_matrix(), (1, len(x.columns))), columns=x.columns)
        f_rec = lambda x: pd.DataFrame(np.reshape(drawdown(x)['Longest_recovery'].as_matrix(), (1, len(x.columns))), columns=x.columns)    
        boot_sample, _ = generate_boot_sample([f_mean, f_var, f_skew, f_kurt, f_dd, f_rec, f_VaR], returns, n_boot=99)
        
        mean_err = boot_sample[0].std()
        var_err = boot_sample[1].std()
        skew_err = boot_sample[2].std()
        kurt_err = boot_sample[3].std()
        dd_err = boot_sample[4].std()
        rec_err = boot_sample[5].std()
        VaR_err = boot_sample[6].std()
    else:
        mean_err = { name:0 for name in returns.columns }
        var_err =  { name:0 for name in returns.columns }
        skew_err = { name:0 for name in returns.columns }
        kurt_err = { name:0 for name in returns.columns }
        dd_err =  { name:0 for name in returns.columns }
        rec_err =  { name:0 for name in returns.columns }
        VaR_err = { name:0 for name in returns.columns }
    
    
    print "Name    |       mu          |        var        |       skew         |        ex Kurtosis      |   Normality   | norm@95%cl "
    print "--------------------------------------------------------------------------------------------------------------------------------"
    for asset in returns.columns:
        print "{:8s}| {:6.4f} +/- {:6.4f} | {:6.4f} +/- {:6.4f} | {:+6.4f} +/- {:6.4f} | {:+6.3E} +/- {:6.2E} |   {:6.4E}  |  {:5s}" .format(asset, means[asset], mean_err[asset], var[asset], var_err[asset], skew[asset] , skew_err[asset], kurt[asset], kurt_err[asset], JB[asset], str(is_norm[asset]))
   
    print ""
    print "Name    |       VaR(5%)      |    largest DD     | largest return | longest recovery | annualised return "
    print "-------------------------------------------------------------------------------------------------------------"
    for asset in returns.columns:
        if (uncertainties): print "{:8s}| {:6.4f} +/- {:6.4f} | {:6.4f} +/- {:6.4f} |     {:+6.4f}    | {:5d} +/- {:5d}  | {:6f} +/- {:6f}  " .format(asset, VaR[asset], VaR_err[asset], l_DD[asset], dd_err[asset], l_ret[asset], l_rec[asset].days, rec_err[asset].days,  (1+means[asset])**12 -1,   (1+means[asset]+mean_err[asset])**12 -  (1+means[asset])**12 )
        else : print "{:8s}| {:6.4f} +/- {:6.4f} | {:6.4f} +/- {:6.4f} |     {:+6.4f}    |  {:5d} +/- {:3d}   | {:6f} +/- {:6f}  " .format(asset, VaR[asset], VaR_err[asset], l_DD[asset], dd_err[asset], l_ret[asset], l_rec[asset].days, rec_err[asset],  (1+means[asset])**12 -1,   (1+means[asset]+mean_err[asset])**12 -  (1+means[asset])**12 )

    print "Summary:"
    print "                   Largest return: {:6s} : {:6.4f} +/- {:6.4f}" .format(means.argmax(), means.max(), mean_err[means.argmax()])
    print "                Smallest variance: {:6s} : {:6.4f} +/- {:6.4f}"  .format(var.argmin(), var.min(), var_err[var.argmin()])
    print "                Smallest VaR(5%%): {:6s} : {:5.3f} +/- {:6.4f}"  .format(VaR.argmin(), VaR.min(), VaR_err[VaR.argmin()])
    ratio = means/var
    print "         Highest return/var ratio: {:6s} : {:6.4f} +/- {:6.4f}"  .format(ratio.argmax(), ratio.max(), ratio.max()*np.sqrt((mean_err[ratio.argmax()]/means[ratio.argmax()])**2 + (var_err[ratio.argmax()]/var[ratio.argmax()])**2))
    
    Sratio = (means - risk_free) / var
    Sratio.sort()
    print "  Highest Sharpe's ratio (rf={:3.1f}): {:6s} : {:6.4f} +/- {:6.4f}"  .format(risk_free*100, Sratio.argmax(), Sratio.max(), Sratio.max()*np.sqrt((mean_err[Sratio.argmax()]/means[Sratio.argmax()])**2 + (var_err[Sratio.argmax()]/var[Sratio.argmax()])**2))
    exit()
    
