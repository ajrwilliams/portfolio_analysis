import matplotlib
matplotlib.use('TkAgg') 
import numpy as np
import pandas as pd
import scipy as sc
import pandas.io.data as web
import datetime
import time
from portfolio import *

    
start = datetime.datetime(2010, 1, 1)
end = datetime.datetime(2013, 1, 27)

#prices = get_monthly_data(["gpe.l", "eat.l", "frcl.l"], start)
#prices.to_pickle("test.pkl")

prices = pd.read_pickle("test.pkl")
returns = pd.DataFrame(np.log(1 + prices.pct_change()), columns=prices.columns)

means=returns.mean()
covar=returns.cov()
vars =returns.var()

print means
#print covar
#print vars

x = min_variance_portfolio(means, covar, returns.columns, False)
print "minimum variance no shorts: ", x
x = min_variance_portfolio(means, covar,returns.columns, True)
print "minimum variance with shorts: ", x
x = portfolio_by_return(means, covar, 0.0095, shortsales=False)
print "next portfolio no shorts: ", x
x = portfolio_by_return(means, covar, 0.0095, shortsales=True)
print "next portfolio with shorts: ", x

f_mean = lambda x: x.mean()
f_var = lambda x: x.var()
tmp = generate_boot_sample([f_mean, f_var], returns, n_boot=5999)
boot_mean = tmp[0]
boot_var = tmp[1]
mean_errors = np.sqrt(boot_mean.var())


#boot_var = generate_boot_sample(f_var, returns, n_boot=9999)

var_errors = np.sqrt(boot_var.var())

print "true returns = ", means
print "boot strap = ", boot_mean.median()

print "true var = ", vars
print"boot strap = ", boot_var.median()

risk_return_contour_plot(boot_mean, boot_var, savefig=None, showfig=False)
risk_return_asset_plot(means, vars, mean_errors=mean_errors, var_errors=var_errors, errors=True, savefig="test_2.png")
  
    
    
